package entity;

import main.CollisionChecker;
import main.GamePanel;
import main.KeyHandler;
import main.UtilityTool;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

public class Player extends Entity {

    GamePanel gp;
    KeyHandler keyH;

    public final int screenX;
    public final int screenY;
    public int hasKey = 0;
    public int pixelCounter = 0;
    public boolean moving = false;

    public Player(GamePanel gp, KeyHandler keyH) {
        this.gp = gp;
        this.keyH = keyH;
        this.screenX = gp.screenWidth/2 - (gp.tileSize/2);
        this.screenY = gp.screenHeight/2 - (gp.tileSize/2);

        solidArea = new Rectangle();
        solidArea.x = 1;//8;
        solidArea.y = 1;//16;
        solidArea.width = 46;//32;
        solidArea.height = 46;//28;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;

        setDefaultValues();
        getPlayerImage();
    }
 // MOVING TILE PER TILE
    public void update() {

        if (!moving) {
            if (keyH.downPressed || keyH.rightPressed || keyH.leftPressed || keyH.upPressed) {

                if (keyH.downPressed) {
                    direction = "down";
                } else if (keyH.rightPressed) {
                    direction = "right";
                } else if (keyH.leftPressed) {
                    direction = "left";
                } else if (keyH.upPressed) {
                    direction = "up";
                }
                moving = true;

                //CHECK TILE COLLISION
                collisionOn = false;
                gp.cChecker.checkTile(this);

                //CHECK OBJECT COLLISION
                int indexObject = gp.cChecker.checkObject(this, true);
                pickObject(indexObject);
            } else {
                standCounter++;
                if (standCounter == 20) {
                    spriteNum = 1;
                    standCounter = 0;
                }
            }
        }
        if (moving) {
            // IF COLLISION IS FALSE PLAYER CAN MOVE
            if (collisionOn == false) {
                switch (direction) {
                    case "up":
                        worldY -= speed;
                        break;
                    case "down":
                        worldY += speed;
                        break;
                    case "left":
                        worldX -= speed;
                        break;
                    case "right":
                        worldX += speed;
                        break;
                }

            }
            spriteCounter++;

            // Every 12 refresh we update our character position 1 to 2
            if (spriteCounter > 12) {
                if (spriteNum == 1) {
                    spriteNum = 2;
                } else if (spriteNum == 2) {
                    spriteNum = 1;
                }
                spriteCounter = 0;
            }
            pixelCounter += speed;

            if (pixelCounter == 48) {
                moving = false;
                pixelCounter = 0;
            }
        }

    }
    // MOVING NORMAL
/*    public void update() {

        if (keyH.downPressed|| keyH.rightPressed || keyH.leftPressed || keyH.upPressed){

            if (keyH.downPressed) {
                direction = "down";
            } else if (keyH.rightPressed) {
                direction = "right";
            } else if (keyH.leftPressed) {
                direction = "left";
            } else if (keyH.upPressed) {
                direction = "up";
            }


            collisionOn = false;
            gp.cChecker.checkTile(this);
            int indexObject = gp.cChecker.checkObject(this,true);
            pickObject(indexObject);

            // IF COLLISION IS FALSE PLAYER CAN MOVE
            if (collisionOn == false){
                switch (direction){
                    case"up":
                        worldY -= speed;
                        break;
                    case"down":
                        worldY += speed;
                        break;
                    case"left":
                        worldX -= speed;
                        break;
                    case"right":
                        worldX += speed;
                        break;
                }

            }
            spriteCounter++;

        // Every 12 refresh we update our character position 1 to 2
        if (spriteCounter > 12 ) {
                if (spriteNum == 1) {
                    spriteNum = 2;
                } else if (spriteNum == 2) {
                    spriteNum = 1;
                }
                spriteCounter = 0;
            }
        }
        else {
            standCounter++;
            if (standCounter == 20) {
                spriteNum = 1;
                standCounter = 0;
            }
        }
    }*/

    public void pickObject(int i){
        if (i != 999){
            String objectName = gp.obj[i].name;

            switch (objectName){
                case "Key":
                    gp.playSE(1);
                    hasKey++;
                    gp.obj[i] = null;
                    gp.ui.showMessage("You got a key !");
                break;
                case "Door":
                    if (hasKey > 0){
                        gp.playSE(4);
                        hasKey--;
                        gp.obj[i] = null;
                        gp.ui.showMessage("Door open !");
                    }else
                        gp.ui.showMessage("You need a key !");
                    break;
                case "Boots":
                    gp.playSE(3);
                    this.speed += 2;
                    gp.obj[i] = null;
                    gp.ui.showMessage("Speed up !");
                    break;
                case "Chest":
                    gp.ui.gameFinished = true;
                    gp.stopSound();
                    gp.playSE(2);
                    break;
                case "Senzu":
                    gp.playSE(3);
                    gp.obj[i] = null;
                    gp.ui.showMessage("You got a Senzu !");
                    break;
            }
        }
    }

    public void setDefaultValues(){
        worldX = gp.tileSize*23;
        worldY = gp.tileSize*21;
        speed = 4;
        direction = "down";
    }

    public void getPlayerImage(){

        up1    = setUp("boyUp1");
        up2    = setUp("boyUp2");
        right1 = setUp("boyRight1");
        right2 = setUp("boyRight2");
        left1  = setUp("boyLeft1");
        left2  = setUp("boyLeft2");
        down1  = setUp("boyDown1");
        down2  = setUp("boyDown2");

    }
    public BufferedImage setUp(String imageName){
        UtilityTool uTool =  new UtilityTool();
        BufferedImage image = null;

        try {
            image = ImageIO.read(getClass().getResourceAsStream("/player/"+imageName+".png"));
            image = uTool.scaleImage(image,gp.tileSize,gp.tileSize);
        }catch (IOException e){
            e.printStackTrace();
        }
        return image;
    }

    public void draw(Graphics2D g2){
        /*g2.setColor(Color.white);
        g2.fillRect(worldX,worldY,gp.tileSize,gp.tileSize);*/
        Random random = new Random();
        BufferedImage image = null;
        // Display our player in different positions a according directions
        switch (direction){
            case"up":
                if (spriteNum == 1) {
                    image = up1;
                }
                if (spriteNum == 2){
                    image = up2;
                }
                break;
            case"down":
                if (spriteNum == 1) {
                    image = down1;
                }
                if (spriteNum == 2){
                    image = down2;
                }
                break;
            case"left":
                if (spriteNum == 1) {
                    image = left1;
                }
                if (spriteNum == 2){
                    image = left2;
                }
                break;
            case"right":
                if (spriteNum == 1) {
                    image = right1;
                }
                if (spriteNum == 2){
                    image = right2;
                }
                break;
        }
        g2.drawImage(image,screenX,screenY,gp.tileSize, gp.tileSize, null);
        g2.setColor(Color.red);
        //g2.drawRect(screenX,screenY,gp.tileSize,gp.tileSize);
    }
}
