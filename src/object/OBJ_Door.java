package object;

import main.GamePanel;

import java.io.IOException;
import javax.imageio.ImageIO;

public class OBJ_Door extends SuperObject {

    GamePanel gp;

    public OBJ_Door(GamePanel gp) {
        this.name = "Door";
        this.gp = gp;

        try {
            this.image = ImageIO.read(this.getClass().getResourceAsStream("/objects/door.png"));
            uTool.scaleImage(image,gp.tileSize,gp.tileSize);
        } catch (IOException var2) {
            var2.printStackTrace();
        }
        this.collision = true;
    }
}
