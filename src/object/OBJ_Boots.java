package object;

import main.GamePanel;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Boots extends SuperObject{

    GamePanel gp;

    public OBJ_Boots(GamePanel gp){
        this.name = "Boots";
        this.gp = gp;

        try {
            this.image = ImageIO.read(this.getClass().getResourceAsStream("/objects/boots.png"));
            uTool.scaleImage(image,gp.tileSize,gp.tileSize);
        } catch (IOException var2) {
            var2.printStackTrace();
        }
        this.collision = true;
    }
}
