package object;

import main.GamePanel;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Chest extends SuperObject{

    GamePanel gp;

    public OBJ_Chest(GamePanel gp){
        this.name = "Chest";
        this.gp = gp;

        try {
            this.image = ImageIO.read(this.getClass().getResourceAsStream("/objects/chest.png"));
            uTool.scaleImage(image,gp.tileSize,gp.tileSize);
        } catch (IOException var2) {
            var2.printStackTrace();
        }
        this.collision = false;
    }
}
