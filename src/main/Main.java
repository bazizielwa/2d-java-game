package main;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        JFrame frame = new JFrame();
        GamePanel gamePanel = new GamePanel();

        frame.add(gamePanel);

        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("2d Adventure");
        frame.setLocationRelativeTo(null);

        frame.pack(); // cause resize the panel and we need to use pack() to a better resolution
        frame.setVisible(true);

        gamePanel.setupGame();
        gamePanel.startGameThread(); // Commence la boucle
    }
}
