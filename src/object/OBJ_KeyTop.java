package object;

import main.GamePanel;

import java.io.IOException;
import javax.imageio.ImageIO;

public class OBJ_KeyTop extends SuperObject {

    public OBJ_KeyTop() {
        this.name = "Key";

        try {
            this.image = ImageIO.read(this.getClass().getResourceAsStream("/objects/key.png"));
            uTool.scaleImage(image,48,48);
        } catch (IOException var2) {
            var2.printStackTrace();
        }
        this.collision = false;
    }
}
