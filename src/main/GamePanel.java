package main;

import entity.Player;
import object.SuperObject;
import org.omg.CORBA.PUBLIC_MEMBER;
import tile.TileManager;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutionException;

public class GamePanel extends JPanel implements Runnable{

    final int originalTileSize = 16; // Standard size tile 16*16
    final int scale = 3; // Zoom our image

    public final int tileSize = originalTileSize * scale; // 48*48
    public final int maxScreenCol = 16;
    public final int maxScreenRow = 12;

    // World settings
    public final int maxWorldCol = 50;
    public final int maxWorldRow = 50;

    public int screenWidth = tileSize * maxScreenCol;  //longueur 768 pixels
    public int screenHeight = tileSize * maxScreenRow; //hauteur 576 pixels
    int FPS = 60;

    TileManager tileM = new TileManager(this);
    Thread gameThread; // Like timer here we can loop the game
    KeyHandler keyH = new KeyHandler(this);
    Sound sound = new Sound();
    Sound se = new Sound();
    public UI ui = new UI(this);
    public AssetSetter aSetter = new AssetSetter(this);
    public Player player = new Player(this,keyH);
    public CollisionChecker cChecker = new CollisionChecker(this);
    public SuperObject  obj[] = new SuperObject[10];

    // GAME STATE
    public int gameState;
    public final int playState = 1;
    public final int pauseState = 2;

    GamePanel(){

        this.setPreferredSize(new Dimension(screenWidth, screenHeight));
        this.setBackground(Color.black);
        this.setDoubleBuffered(true); // Enabling this can improve game's rendering performance
        this.addKeyListener(keyH);
        this. setFocusable(true); // With this, this main.GamePanel can be "Focused" to receive key input
    }
    public void setupGame (){
        aSetter.setObject();
        playMusic(0);
    }

    public void startGameThread(){

        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void run() {
        // Creating a loop game to recall update() and repaint() 60x per sec
        double drawInterval = (double)(1000000000 / this.FPS);
        double delta = 0.0D;
        long lastTime = System.nanoTime();

        while(this.gameThread != null) {
            long currentTime = System.nanoTime(); //1 000 000 000 nanosec = 1sec / current time = le temps actuel dans le systeme de la machine
            delta += (double)(currentTime - lastTime) / drawInterval;
            lastTime = currentTime;
            if (delta >= 1.0D) {
                this.update();
                this.repaint();
                --delta;
            }
        }
    }

    public void update(){
            player.update();
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        /*BufferedImage backI = null;
        try {
            backI = ImageIO.read(getClass().getResourceAsStream("player/backgroundImage.png"));
            g.drawImage(backI, 0, 0, null);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        Graphics2D g2 = (Graphics2D) g; //Pour avoir plus d'options graphique

        long drawStart = 0;
        if (keyH.checkDrawTime){
            drawStart = System.nanoTime();
        }

        //Tile
        tileM.draw(g2);

        // Drawing obj
        for (int i = 0; i < obj.length; i++) {
            if (obj[i] != null){
                obj[i].draw(g2,this);
            }
        }
        // Drawing player
        player.draw(g2);

        // Drawing graphics string
        ui.draw(g2);

        // DEBUG
        if (keyH.checkDrawTime == true){
            long drawEnd = System.nanoTime();
            long passed = drawEnd - drawStart;
            g2.setColor(Color.white);
            g2.drawString("Draw time: " +passed, 10,400);
            System.out.println("Draw Time: " +passed);
        }

        g2.dispose();
    }

    public void playMusic(int i){

        sound.setFile(i);
        sound.play();
        sound.loop();
    }

    public void stopSound(){
        sound.stop();
    }

    public void playSE(int i){
        se.setFile(i);
        se.play();
    }
}
