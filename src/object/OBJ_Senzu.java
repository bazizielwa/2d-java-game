package object;

import main.GamePanel;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Senzu extends SuperObject{

    GamePanel gp;

    public OBJ_Senzu(GamePanel gp) {
        this.name = "Senzu";
        this.gp = gp;

        try {
            this.image = ImageIO.read(this.getClass().getResourceAsStream("/objects/senzu.png"));
            uTool.scaleImage(image,gp.tileSize,gp.tileSize);
        } catch (IOException var2) {
            var2.printStackTrace();
        }
        this.collision = false;
    }
}
